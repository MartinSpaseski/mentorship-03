<?php
$users = file_get_contents("https://jsonplaceholder.typicode.com/users");
$posts = file_get_contents("https://jsonplaceholder.typicode.com/posts");
$todos = file_get_contents("https://jsonplaceholder.typicode.com/todos");

$users = json_decode($users, true);
$posts = json_decode($posts, true);
$todos = json_decode($todos, true);

$combined_array = [];
foreach ($users as $user) {
  $user_info = [
    'info' => [
      'name' => $user['name'],
      'username' => $user['username'],
      'email' => $user['email'],
      'city' => $user['address']['city'],
      'company' => $user['company']['name']
    ],
    'posts' => [],
    'todos' => []
  ];

  foreach ($posts as $post) {
    if ($post['userId'] == $user['id']) {
      $user_info['posts'][] = [
        'title' => $post['title'],
        'body' => $post['body']
      ];
    }
  }

  foreach ($todos as $todo) {
    if ($todo['userId'] == $user['id']) {
      $user_info['todos'][] = [
        'title' => $todo['title'],
        'completed' => $todo['completed']
      ];
    }
  }

  $combined_array[] = $user_info;
}
