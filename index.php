<?php

require_once __DIR__ . '/json_decoded.php';


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link rel="stylesheet" href="./style.css">
</head>

<body>
  <?php
  foreach ($combined_array as $user_data) {
    echo " <div class=\"container\">";

    // LEFT
    echo "<div class=\"left\">";
    echo "<h2>" .  $user_data['info']['name'] . "</h2>";
    echo "<h2>@" .  $user_data['info']['username'] . "</h2>";
    echo "Email: " . $user_data['info']['email'] . "<br>";
    echo "City: " . $user_data['info']['city'] . "<br>";
    echo "Company: " . $user_data['info']['company'] . "<br>";
    echo "</div>";


    // CENTER
    echo "<div class=\"center\">";
    echo "<h2>Posts</h2>";
    foreach ($user_data['posts'] as $post) {
      echo "Title: " . $post['title'] . "<br>";
      echo "Body: " . $post['body'] . "<br>";
      echo "<hr>";
    }
    echo "</div>";


    // RIGHT
    echo "<div class=\"right\">";
    echo "<h2>Todos</h2>";
    foreach ($user_data['todos'] as $todo) {
      if ($todo['completed']) {
        echo "<p class=\"done\">" . $todo['title'] . "</p>";
      } else {
        echo "<p class=\"in_progress\">" . $todo['title'] . "</p>";
      };
    }
    echo "</div>";
    echo "</div>";
  }
  ?>
</body>

</html>